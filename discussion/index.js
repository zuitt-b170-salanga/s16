/*
	Repetition Control Structures
		Loops - executes codes repeatedly in a pre-set number of times or forever
*/

// While loop - takes in an expression/condition before proceeding in the evaluation of the codes


/*

	while
*/
let count = 5;

while(count!==0){
	console.log("While loop:" + count);

	count--
};

/*
	5 4 3 2 1 0
*/

let countTwo = 1;

while(countTwo<11){
	console.log(countTwo);

	countTwo ++;
}

// do-while loop - at least 1 code block to be executed before proceeding to the condition 
	
/*
	do{
		statement/s
	}while(condition)
*/

let countThree = 5;

do{
	console.log("Do-While Loop:" + countThree);

	countThree --;
}while(countThree > 0);


let countFour = 1;

do{
	console.log(countFour);

	countFour++;
}while (countFour < 11)


// for loop - more flexible looping
	/*
		SYNTAX
			for(initialization; condition; finalExpression){
				statement/s;
			}
		PROCESS
			1. Initialization - recognizing the variable created
			2. Condition - reading the condition
			3. Statement/s - executing the commands/statements
			4. finalExpression - executing the finalExpression
	*/

for(let countFive = 5; countFive > 0; countFive--){
	console.log("For Loop:" + countFive);
}

let number = Number(prompt("Give me a Number"));
for (let numCount = 1; numCount <= number; numCount++){
	console.log("Hello Batch 170")
}


let myName = "alex";
/*
.length - representing the number of characters inside the string; the characters are all that are 
 	included in the string(spaces, symbols, punctuations)
console.log(myName.length);


variable[x] - accessing the array/string of the variable; 0-based - the counting of the elements starts 
	at 0  
console.log(myName[2]);
*/

for(let x=0; x < myName.length;x++){
	console.log(myName[x])
}
// *************
// a
// index.js:95 l
// index.js:95 e
// index.js:95 x
// *************




myName = "Alex";
for (let i = 0; i < myName.length; i++){
		// toLowerCase() - converts every character into lower case, regardless if they are originally
		// 	uppercase
	if(
		myName[i].toLowerCase()=="a"||
		myName[i].toLowerCase()=="e"||
		myName[i].toLowerCase()=="i"||
		myName[i].toLowerCase()=="o"||
		myName[i].toLowerCase()=="u"
		) {
		console.log(3);
		}
	else{
		console.log(myName[i]);
	}
}
// *************
// 3
// index.js:112 l
// index.js:109 3
// index.js:112 x
// *************



// Continue & Break Statements

	for(let countSix = 0; countSix <= 20; countSix++){
		if (countSix %2 === 0) {
			continue;  //tells the browser/codes to continue to the nxt iteration of the loop
		}
		console.log("Continue and Break:" + countSix);
		if(countSix>10){
			break;  //tells the browser/codes to terminate the loop even if the condition is met/
			//satisfied;
		}
	}

// ******************* entered 11
// Continue and Break:1
// index.js:122 Continue and Break:3
// index.js:122 Continue and Break:5
// index.js:122 Continue and Break:7
// index.js:122 Continue and Break:9
// index.js:122 Continue and Break:11
// *******************
/*
countSix = 0; 0 <= 20; countSix++ (`1`)
	if (countSix %2 === 0) ===> 0/2===0
			continue; 

countSix = 1; 1 <= 20; countSix++ (`2`)
	if (countSix %2 === 0) ===> 1/2===0.5
	console.log("Continue and Break:" + 1);

countSix = 2; 2 <= 20; countSix++ (`3`)
	if (countSix %2 === 0) ===> 2/2===0
			continue; 

countSix = 3; 3 <= 20; countSix++ (`4`)
	if (countSix %2 === 0) ===> 3/2=== 1.5
	console.log("Continue and Break:" + 3);

countSix = 4; 4 <= 20; countSix++ (`5`)
	if (countSix %2 === 0) ===> 4/2===2.0
			continue; 

.
.
.

countSix = 11; 11 <= 20; countSix++ (`12`)
	if (countSix %2 === 0) ===> 11/2=== 5.5
		if(countSix>10){
			break; 
*/			



let name = "Alexandro";
// name has 8 indices(name[index]) but 9 elements(.length)

/*
	compute for index: .length -1
	compute for .length: index +1
*/
for (let i = 0; i < name.length; i++){
	console.log( name[i] );

	if (name[i].toLowerCase() === "a") {
		console.log("Continue to the next Iteration");
		continue;
	}
	if (name[i].toLowerCase() === "d") {
		break
	}
}

/*


****************
entered 9
A
index.js:136 Continue to the next Iteration
index.js:133 l
index.js:133 e
index.js:133 x
index.js:133 a
index.js:136 Continue to the next Iteration
index.js:133 n
index.js:133 d
****************


let name = "Alexandro";

i=0(A); 0 < 9(name.length); +1===1
	console.log( name[0] ); === `A`
	console.log("Continue to the next Iteration");
		continue;

i=1(l); 1 < 9(name.length); +1===2
	console.log( name[1] ); === `l`

i=2(e); 2 < 9(name.length); +1===3
	console.log( name[2] ); === `e`

i=3(x); 3 < 9(name.length); +1===4
	console.log( name[3] ); === `x`

i=4(a); 4 < 9(name.length); +1===5
	console.log( name[4] ); === `a`
	console.log("Continue to the next Iteration");
		continue;

i=5(x); 5 < 9(name.length); +1===6
	console.log( name[5] ); === `n`

i=6(n); 6 < 9(name.length); +1===7
	console.log( name[] ); === `d`
if (name[i].toLowerCase() === "d") {
		break
*/




