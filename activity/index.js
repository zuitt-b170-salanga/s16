console.log("Hello, World!!!");

const userNum = parseInt(prompt("Enter a Number: "));
console.log("The number you provided is " + userNum);

for (let i = userNum; i >=0; i--){
	if (i <=50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}

	if ((i>=51) && (i%10===0)){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	
	if ((i>=51) && (i%10===5)){
		console.log(i);
		continue;
	}
	
}
/* ================================= */

let wordString = "supercalifragilisticexpialidocious"; //34 letters; 16 vowels; 18 consonants
//let consString;

console.log(wordString);
for (let j = 0; j < wordString.length; j++){
	if(
		wordString[j]=="a"||
		wordString[j]=="e"||
		wordString[j]=="i"||
		wordString[j]=="o"||
		wordString[j]=="u"
		) 
	{
		continue;
	}
	else {
			let consString = wordString[j];
			console.log(consString);
		}		
		
	}		
		
/* ================================= */
